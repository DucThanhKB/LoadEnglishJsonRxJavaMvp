package com.ducthanh.loadjsonenglish.activity.paragraphActivity;

import com.ducthanh.loadjsonenglish.data.EnglishFactory;
import com.ducthanh.loadjsonenglish.data.EnglishResponse;
import com.ducthanh.loadjsonenglish.data.EnglishService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by DucThanh on 7/19/2017.
 */

public class ParagraphModel {
    ParagraphPresenter mParagraphPresenter;

    public ParagraphModel(ParagraphPresenter mParagraphPresenter) {
        this.mParagraphPresenter = mParagraphPresenter;
    }

    public void getData(String url) {
        EnglishFactory englishFactory = new EnglishFactory();
        EnglishService englishService = englishFactory.create();
        Disposable disposable = englishService.fetchEnglish(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<EnglishResponse>() {
                    @Override
                    public void accept(EnglishResponse englishResponse) throws Exception {
                        mParagraphPresenter.updateView(englishResponse.getData().getTitleParagraph());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                    }
                });
    }
}
