package com.ducthanh.loadjsonenglish.activity.mainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ducthanh.loadjsonenglish.R;
import com.ducthanh.loadjsonenglish.activity.dialogActivity.DialogActivity;
import com.ducthanh.loadjsonenglish.activity.paragraphActivity.ParagraphActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_load_title_dialog)
    public void buttonLoadTitledDialogPressed() {
        Intent intent =  new Intent(this,DialogActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.button_load_title_paragraph)
    public void buttonLoadTitleParagraph() {
        Intent intent =  new Intent(this,ParagraphActivity.class);
        startActivity(intent);
    }
}
