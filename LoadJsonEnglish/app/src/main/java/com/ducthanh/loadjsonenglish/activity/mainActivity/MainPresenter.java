package com.ducthanh.loadjsonenglish.activity.mainActivity;

import com.ducthanh.loadjsonenglish.data.EnglishResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by DucThanh on 7/19/2017.
 */

public class MainPresenter implements MainInterface.MainPresenterInterface {
    private MainModel mMainModel;
    private MainInterface.MainViewInterface mMainViewInterface;

    public MainPresenter(MainInterface.MainViewInterface mMainViewInterface) {
        this.mMainViewInterface = mMainViewInterface;
        mMainModel = new MainModel(this);
    }

}
