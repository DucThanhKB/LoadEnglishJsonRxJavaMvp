package com.ducthanh.loadjsonenglish.activity.dialogActivity;

import com.ducthanh.loadjsonenglish.model.TitleDialog;

import java.util.List;

/**
 * Created by DucThanh on 7/19/2017.
 */

public interface DialogInterface {

    interface DialogViewInterface {
        void showData(List<TitleDialog> titleDialogList);
    }

    interface DialogPresenterInterface {
        void updateView(List<TitleDialog> titleDialogList);
    }

}
