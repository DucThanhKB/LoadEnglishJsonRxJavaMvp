package com.ducthanh.loadjsonenglish.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ducthanh.loadjsonenglish.R;
import com.ducthanh.loadjsonenglish.model.TitleParagraph;

import java.util.List;

/**
 * Created by DucThanh on 6/19/2017.
 */

public class ParagraphAdapter extends RecyclerView.Adapter<ParagraphAdapter.ViewHolder> {

    List<TitleParagraph> listTitleParagraph;
    private Context mContext;
    // Define listener member variable
    private OnItemClickListener listener;

    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public ParagraphAdapter(Context context, List<TitleParagraph> listTitleParagraph) {
        this.listTitleParagraph = listTitleParagraph;
        mContext = context;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View itemView = inflater.inflate(R.layout.item, parent, false);
        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;

    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String id = listTitleParagraph.get(position).getParagraphId();
        holder.textId.setText(id);
        String name = listTitleParagraph.get(position).getName();
        holder.textName.setText(name);
    }

    @Override
    public int getItemCount() {
        return listTitleParagraph.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView textId;
        public TextView textName;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(final View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            textId = (TextView) itemView.findViewById(R.id.text_id);
            textName = (TextView) itemView.findViewById(R.id.text_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(itemView, position);
                        }
                    }
                }
            });
        }
    }
}
