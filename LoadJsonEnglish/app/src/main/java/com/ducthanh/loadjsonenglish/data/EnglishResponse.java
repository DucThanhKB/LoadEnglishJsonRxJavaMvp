package com.ducthanh.loadjsonenglish.data;

import com.ducthanh.loadjsonenglish.model.Data;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DucThanh on 7/19/2017.
 */

public class EnglishResponse {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}

