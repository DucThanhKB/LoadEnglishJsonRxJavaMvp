package com.ducthanh.loadjsonenglish.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DucThanh on 7/19/2017.
 */

public class TitleParagraph implements Parcelable{

    @SerializedName("paragraph_id")
    @Expose
    private String paragraphId;
    @SerializedName("name")
    @Expose
    private String name;

    protected TitleParagraph(Parcel in) {
        paragraphId = in.readString();
        name = in.readString();
    }

    public static final Creator<TitleParagraph> CREATOR = new Creator<TitleParagraph>() {
        @Override
        public TitleParagraph createFromParcel(Parcel in) {
            return new TitleParagraph(in);
        }

        @Override
        public TitleParagraph[] newArray(int size) {
            return new TitleParagraph[size];
        }
    };

    public String getParagraphId() {
        return paragraphId;
    }

    public void setParagraphId(String paragraphId) {
        this.paragraphId = paragraphId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(paragraphId);
        dest.writeString(name);
    }
}
