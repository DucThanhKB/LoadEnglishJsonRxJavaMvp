package com.ducthanh.loadjsonenglish.data;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by DucThanh on 7/19/2017.
 */

public interface EnglishService {
    @GET
    Observable<EnglishResponse> fetchEnglish(@Url String url);
}
