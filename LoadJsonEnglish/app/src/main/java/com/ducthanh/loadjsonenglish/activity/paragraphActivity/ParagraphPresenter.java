package com.ducthanh.loadjsonenglish.activity.paragraphActivity;

import com.ducthanh.loadjsonenglish.model.TitleParagraph;

import java.util.List;

/**
 * Created by DucThanh on 7/19/2017.
 */

public class ParagraphPresenter implements ParagraphInterface.ParagraphPresenterInterface {

    private ParagraphModel mParagraphModel;
    private ParagraphInterface.ParagraphViewInterface mParagrapViewInterface;

    public ParagraphPresenter(ParagraphInterface.ParagraphViewInterface mParagrapViewInterface) {
        this.mParagrapViewInterface = mParagrapViewInterface;
        mParagraphModel = new ParagraphModel(this);
    }

    public void hanleLoadTitleParagraph(String url) {
        mParagraphModel.getData(url);
    }

    @Override
    public void updateView(List<TitleParagraph> titleParagraphList) {
        mParagrapViewInterface.showData(titleParagraphList);
    }
}
