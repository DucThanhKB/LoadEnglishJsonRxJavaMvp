package com.ducthanh.loadjsonenglish.activity.paragraphActivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ducthanh.loadjsonenglish.R;
import com.ducthanh.loadjsonenglish.adapter.ParagraphAdapter;
import com.ducthanh.loadjsonenglish.data.EnglishFactory;
import com.ducthanh.loadjsonenglish.model.TitleParagraph;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParagraphActivity extends AppCompatActivity implements ParagraphInterface.ParagraphViewInterface {

    @BindView(R.id.recycler_paragraph)
    RecyclerView recyclerParagraph;

    private ParagraphPresenter mParagraphPresenter;
    ParagraphAdapter paragraphAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paragraph_activity);
        ButterKnife.bind(this);
        mParagraphPresenter =  new ParagraphPresenter(this);
        mParagraphPresenter.hanleLoadTitleParagraph(EnglishFactory.PROJECT_URL);
    }

    @Override
    public void showData(List<TitleParagraph> titleParagraphList) {
        paragraphAdapter = new ParagraphAdapter(this,titleParagraphList);
        recyclerParagraph.setAdapter(paragraphAdapter);
        LinearLayoutManager linearLayoutManager =  new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        linearLayoutManager.scrollToPosition(0);
        recyclerParagraph.setLayoutManager(linearLayoutManager);
    }
}
