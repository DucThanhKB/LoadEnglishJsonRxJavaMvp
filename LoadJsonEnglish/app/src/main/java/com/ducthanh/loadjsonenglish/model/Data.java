package com.ducthanh.loadjsonenglish.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DucThanh on 7/19/2017.
 */

public class Data {

    @SerializedName("data_version")
    @Expose
    private Integer dataVersion;
    @SerializedName("show_ad")
    @Expose
    private Integer showAd;
    @SerializedName("sumSimple")
    @Expose
    private List<SumSimple> sumSimple = null;
    @SerializedName("titleDialog")
    @Expose
    private List<TitleDialog> titleDialog = null;
    @SerializedName("titleParagraph")
    @Expose
    private List<TitleParagraph> titleParagraph = null;

    public Integer getDataVersion() {
        return dataVersion;
    }

    public void setDataVersion(Integer dataVersion) {
        this.dataVersion = dataVersion;
    }

    public Integer getShowAd() {
        return showAd;
    }

    public void setShowAd(Integer showAd) {
        this.showAd = showAd;
    }

    public List<SumSimple> getSumSimple() {
        return sumSimple;
    }

    public void setSumSimple(List<SumSimple> sumSimple) {
        this.sumSimple = sumSimple;
    }

    public List<TitleDialog> getTitleDialog() {
        return titleDialog;
    }

    public void setTitleDialog(List<TitleDialog> titleDialog) {
        this.titleDialog = titleDialog;
    }

    public List<TitleParagraph> getTitleParagraph() {
        return titleParagraph;
    }

    public void setTitleParagraph(List<TitleParagraph> titleParagraph) {
        this.titleParagraph = titleParagraph;
    }
}
