package com.ducthanh.loadjsonenglish.activity.dialogActivity;

import com.ducthanh.loadjsonenglish.model.TitleDialog;

import java.util.List;

/**
 * Created by DucThanh on 7/19/2017.
 */

public class DialogPresenter implements DialogInterface.DialogPresenterInterface{
    private DialogModel mDialogModel;
    private DialogInterface.DialogViewInterface mDialodView;
    public DialogPresenter(DialogInterface.DialogViewInterface mDialodView){
        this.mDialodView = mDialodView;
        mDialogModel =  new DialogModel(this);
    }
    public void hanleLoadTitleDialog(String url){
        mDialogModel.getData(url);
    }
    public void updateView(List<TitleDialog> titleDialogList){
        mDialodView.showData(titleDialogList);
    }
}
