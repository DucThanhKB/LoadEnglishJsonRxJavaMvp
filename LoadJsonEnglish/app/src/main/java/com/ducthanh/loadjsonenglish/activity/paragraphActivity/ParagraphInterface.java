package com.ducthanh.loadjsonenglish.activity.paragraphActivity;

import com.ducthanh.loadjsonenglish.model.TitleParagraph;

import java.util.List;

/**
 * Created by DucThanh on 7/19/2017.
 */

public interface ParagraphInterface {
    interface ParagraphViewInterface {
        void showData(List<TitleParagraph> titleParagraphList);
    }
    interface ParagraphPresenterInterface {
        void updateView(List<TitleParagraph> titleParagraphList);
    }
}
