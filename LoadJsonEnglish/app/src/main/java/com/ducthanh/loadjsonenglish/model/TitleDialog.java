package com.ducthanh.loadjsonenglish.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DucThanh on 7/19/2017.
 */

public class TitleDialog implements Parcelable {

    @SerializedName("dialog_id")
    @Expose
    private String dialogId;
    @SerializedName("name")
    @Expose
    private String name;

    protected TitleDialog(Parcel in) {
        dialogId = in.readString();
        name = in.readString();
    }

    public static final Creator<TitleDialog> CREATOR = new Creator<TitleDialog>() {
        @Override
        public TitleDialog createFromParcel(Parcel in) {
            return new TitleDialog(in);
        }

        @Override
        public TitleDialog[] newArray(int size) {
            return new TitleDialog[size];
        }
    };

    public String getDialogId() {
        return dialogId;
    }

    public void setDialogId(String dialogId) {
        this.dialogId = dialogId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dialogId);
        dest.writeString(name);
    }
}
