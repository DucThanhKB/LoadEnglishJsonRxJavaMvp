package com.ducthanh.loadjsonenglish.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DucThanh on 7/19/2017.
 */

public class SumSimple {
    @SerializedName("sumSimple")
    @Expose
    private String sumSimple;

    public String getSumSimple() {
        return sumSimple;
    }

    public void setSumSimple(String sumSimple) {
        this.sumSimple = sumSimple;
    }
}
