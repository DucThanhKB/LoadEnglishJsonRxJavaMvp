package com.ducthanh.loadjsonenglish.activity.dialogActivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ducthanh.loadjsonenglish.R;
import com.ducthanh.loadjsonenglish.adapter.DialogAdapter;
import com.ducthanh.loadjsonenglish.data.EnglishFactory;
import com.ducthanh.loadjsonenglish.model.TitleDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogActivity extends AppCompatActivity implements DialogInterface.DialogViewInterface {

    @BindView(R.id.recycler_dialog)
    RecyclerView recyclDialog;

    DialogAdapter dialogAdapter;
    private DialogPresenter mDialogPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        ButterKnife.bind(this);
        mDialogPresenter = new DialogPresenter(this);
        mDialogPresenter.hanleLoadTitleDialog(EnglishFactory.PROJECT_URL);
    }
    @Override
    public void showData(List<TitleDialog> titleDialogList) {
        dialogAdapter = new DialogAdapter(this, titleDialogList);
        recyclDialog.setAdapter(dialogAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.scrollToPosition(0);
        recyclDialog.setLayoutManager(linearLayoutManager);
    }
}
